
(use
  srfi-37
  sdl2-raw lolevel)

; Uses SDL_image IMG_Load instead of SDL_LoadBMP

(define (load-image img)
  (let ([surf
          (or (IMG_Load img)
              (error (SDL_GetError)))])
    (set-finalizer! surf SDL_FreeSurface)
    surf))

(when (< (SDL_Init 1) 0)
  (error (SDL_GetError)))

(unless (= (IMG_Init IMG_INIT_PNG) IMG_INIT_PNG)
  (error (SDL_GetError)))

(define *image* (make-parameter #f))
(define
  file-arg
  (option '(#\i "--image") #t #t
	(lambda (o n x vals)
      (*image* x))))

(args-fold (command-line-arguments) (list file-arg) (lambda _ #t) cons #t)

(let* ([image (if (and (*image*)
					   (file-exists? (*image*)))
                (load-image (*image*))
                (error "Please specify [-i|--image] IMAGE.png"))]
       [win
         (or (SDL_CreateWindow "Hello!"
                               SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
                               (c-field-ref SDL_Surface image w)
                               (c-field-ref SDL_Surface image h)
                               SDL_WINDOW_SHOWN)
             (error (SDL_GetError)))]
       [surf (SDL_GetWindowSurface win)])
  ; SDL_BlitSurface is an alias of SDL_UpperBlit, not exported by sdl2-raw
  (SDL_UpperBlit image #f surf #f)
  (SDL_UpdateWindowSurface win)
  (SDL_Delay 2000)
  (SDL_DestroyWindow win)
  (SDL_Quit))



