
(use
  matchable
  srfi-37
  sdl2-raw lolevel)

; Render some nice shapes
; See <rect-draw> which just houses two SDL_Rect pointers

(when (< (SDL_Init 1) 0)
  (error (SDL_GetError)))

(define-record-type
  <rect-draw>
  (%make-rect-draw fill outline)
  rect-draw?
  (fill rect-draw-fill)
  (outline rect-draw-outline))
(define (make-rect-draw fillv outlinev)
  (define (set-rect vec rect)
    (match
      vec
      [#(x y w h)
       (c-field-set! SDL_Rect rect (x) x)
       (c-field-set! SDL_Rect rect (y) y)
       (c-field-set! SDL_Rect rect (w) w)
       (c-field-set! SDL_Rect rect (h) h)]))
  (let-foreign
    ([fill SDL_Rect]
     [outline SDL_Rect])
    (set-rect fillv fill)
    (set-rect outlinev outline)
    (%make-rect-draw fill outline)))

(define (draw-geometry renderer rect w h)
  (SDL_SetRenderDrawColor renderer #xff #xff #xff #xff)
  (SDL_RenderClear renderer)
  (SDL_SetRenderDrawColor renderer #xff #x00 #x00 #xff)
  (SDL_RenderFillRect renderer (rect-draw-fill rect))
  (SDL_SetRenderDrawColor renderer #x00 #xff #x00 #xff)
  (SDL_RenderDrawRect renderer (rect-draw-outline rect))
  (SDL_SetRenderDrawColor renderer #x00 #x00 #xff #xff)
  (SDL_RenderDrawLine renderer 0 (/ h 2) w (/ h 2))
  (SDL_SetRenderDrawColor renderer #xff #xff #x00 #xff)
  (let draw-loop ([i 0])
    (when (< i h)
      (SDL_RenderDrawPoint renderer (/ w 2) i)
      (draw-loop (+ i 4))))
  (SDL_RenderPresent renderer))

(let* ([win
         (or (SDL_CreateWindow
               "Geometry render"
               SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
               640 480
               SDL_WINDOW_SHOWN)
             (error (SDL_GetError)))]
       [renderer (or (SDL_CreateRenderer win -1 SDL_RENDERER_ACCELERATED)
                     (error (SDL_GetError)))]
       [rect (make-rect-draw
               `#(,(fx/ 640 4)
                   ,(fx/ 480 4)
                   ,(fx/ 640 2)
                   ,(fx/ 480 2))
               `#(,(fx/ 640 6)
                   ,(fx/ 480 6)
                   ,(fx/ (* 640 2) 3)
                   ,(fx/ (* 480 2) 3)))])
  (draw-geometry renderer rect 640 480)
  (let-foreign
    ([ev SDL_Event])
    (let ev-loop ()
      (unless (SDL_WaitEvent ev)
        (error (SDL_GetError)))
      (let ([ety (c-field-ref SDL_Event ev type)])
        (select
          ety
          [(SDL_QUIT) #t]
          [else
            (ev-loop)]))))
  (SDL_DestroyWindow win)
  (SDL_Quit))


