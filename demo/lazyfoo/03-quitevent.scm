
(use
  sdl2-raw lolevel)

; Waits for a quit event with SDL_WaitEvent

(when (< (SDL_Init 1) 0)
  (error (SDL_GetError)))

(let* ([win
         (or (SDL_CreateWindow "Hello!"
                               SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
                               640 480
                               SDL_WINDOW_SHOWN)
             (error (SDL_GetError)))])
  (let-foreign
    ([ev SDL_Event])
    (let ev-loop ()
      (unless (SDL_WaitEvent ev)
        (error (SDL_GetError)))
      (let ([ety (c-field-ref SDL_Event ev type)])
        (format #t "Event type ~a\n" ety)
        (gc)
        (select
          ety
          [(SDL_QUIT)
           (format #t "SDL_QUIT\n")]
          [else (ev-loop)]))))
  (SDL_DestroyWindow win)
  (SDL_Quit))

