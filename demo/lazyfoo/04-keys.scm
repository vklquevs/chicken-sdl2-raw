
(use
  sdl2-raw lolevel)

; Waits for a quit event with SDL_WaitEvent

(when (< (SDL_Init 1) 0)
  (error (SDL_GetError)))

(define (print-key-event down? ev)
  (let ([keysym (c-field-ref SDL_Event ev key keysym sym)])
    (format #t "Key ~a ~a\n" (if down? "down" "up") keysym)))

(let* ([win
         (or (SDL_CreateWindow "Hello!"
                               SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
                               640 480
                               SDL_WINDOW_SHOWN)
             (error (SDL_GetError)))])
  (let-foreign
    ([ev SDL_Event])
    (call/cc
      (lambda (quitk)
        (let ev-loop ()
          (unless (SDL_WaitEvent ev)
            (error (SDL_GetError)))
          (let ([ety (c-field-ref SDL_Event ev type)])
            (format #t "Event type ~a\n" ety)
            (select
              ety
              [(SDL_QUIT)
               (format #t "SDL_QUIT\n")
               (quitk #t)]
              [(SDL_KEYDOWN)
               (print-key-event #t ev)]
              [(SDL_KEYUP)
               (print-key-event #f ev)]
              [else #t])
            (ev-loop))))))
  (SDL_DestroyWindow win)
  (SDL_Quit))

