
(use sdl2-raw lolevel)

(when (< (SDL_Init 1) 0)
  (error (SDL_GetError)))

(let* ([win
         (or (SDL_CreateWindow "Hello!"
                               SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
                               640 480
                               SDL_WINDOW_SHOWN)
             (error (SDL_GetError)))]
       [surf (SDL_GetWindowSurface win)])
  (SDL_FillRect surf #f (SDL_MapRGB (c-field-ref SDL_Surface surf format)
                                    #xff #xff #xff))
  (SDL_UpdateWindowSurface win)
  (SDL_Delay 2000)
  (SDL_DestroyWindow win)
  (SDL_Quit))


