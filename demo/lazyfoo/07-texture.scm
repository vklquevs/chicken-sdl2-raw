
(use
  srfi-37
  sdl2-raw lolevel)

; Like 02 but using accelerated rendering with a renderer and texture
; Also the window is resizable

(define (load-image img)
  (let ([surf
          (or (IMG_Load img)
              (error (SDL_GetError)))])
    (set-finalizer! surf SDL_FreeSurface)
    surf))

(when (< (SDL_Init 1) 0)
  (error (SDL_GetError)))

(unless (= (IMG_Init IMG_INIT_PNG) IMG_INIT_PNG)
  (error (SDL_GetError)))

(define *image* (make-parameter #f))
(define
  file-arg
  (option '(#\i "--image") #t #t
	(lambda (o n x vals)
      (*image* x))))

(args-fold (command-line-arguments) (list file-arg) (lambda _ #t) cons #t)

(define (draw-texture renderer texture)
  (SDL_RenderClear renderer)
  (SDL_RenderCopy renderer texture #f #f)
  (SDL_RenderPresent renderer))

(let* ([image (if (and (*image*)
					   (file-exists? (*image*)))
                (load-image (*image*))
                (error "Please specify [-i|--image] IMAGE.png"))]
       [win
         (or (SDL_CreateWindow
               "Texture render"
               SDL_WINDOWPOS_UNDEFINED SDL_WINDOWPOS_UNDEFINED
               (c-field-ref SDL_Surface image w)
               (c-field-ref SDL_Surface image h)
               (+ SDL_WINDOW_RESIZABLE
                  SDL_WINDOW_SHOWN))
             (error (SDL_GetError)))]
       [renderer (or (SDL_CreateRenderer win -1 SDL_RENDERER_ACCELERATED)
                     (error (SDL_GetError)))]
       [tex (or (SDL_CreateTextureFromSurface renderer image)
                (error (SDL_GetError)))])
  (draw-texture renderer tex)
  (let-foreign
    ([ev SDL_Event])
    (let ev-loop ()
      (unless (SDL_WaitEvent ev)
        (error (SDL_GetError)))
      (let ([ety (c-field-ref SDL_Event ev type)])
        (select
          ety
          [(SDL_QUIT) #t]
          [else
            (draw-texture renderer tex)
            (ev-loop)]))))
  (SDL_DestroyWindow win)
  (SDL_Quit))

