
(use
  srfi-1
  matchable)

(define (sformat fstr . args)
  (string->symbol (apply format #f fstr args)))

(define (eprintf fstr . args)
  (apply format (current-error-port) fstr args))

(define (split-list l at)
  (if (null? l) '()
    (let loop ([acc-all '()]
               [acc-one '()]
               [l l])
      (cond
        [(null? l)
         (reverse (cons (reverse acc-one) acc-all))]
        [(equal? (car l) at)
         (loop (cons (reverse acc-one) acc-all) '() (cdr l))]
        [#t
         (loop acc-all (cons (car l) acc-one) (cdr l))]))))

(define (commas->multiple-fields fld)
  (match
    (split-list fld ",")
    [(one) (list one)] ; no commas
    ; TODO handle more than two
    [((ty a) (b)) `((,ty ,a) (,ty ,b))]
    [fc
      (eprintf "; Not implemented: multiple fields ~s\n" fc)
      '()]))

(define (read-fields name flds)
  (let ([fflds
          (concatenate
            (map commas->multiple-fields
                 (filter (lambda (fld) (not (null? fld)))
                         (split-list flds ";"))))])
    (filter-map
      (match-lambda
        [() #f]
        [(ty nam) (list nam ty)]
        [fld (eprintf "; Ignoring field ~a ~s\n" name fld) #f])
      fflds)))

(define (struct->scm name fields)
  `((define-foreign-structure
      ,name ()
      ,@(map
          (lambda (f) `(,(car f) ,(cadr f) #t))
          fields))))

(define *repeats* (make-hash-table))

(define (typedef-struct->scm sexpr)
  (match
    sexpr
    [((or 'typedef-struct 'typedef-union) nam (flds ...) nam2 ";")
     (if (hash-table-exists? *repeats* nam)
       '()
       (let ([scmstr (struct->scm nam (read-fields nam flds))])
         (hash-table-set! *repeats* nam scmstr)
         scmstr))]
    [((or 'typedef-struct 'typedef-union) (flds ...) nam ";")
     (if (hash-table-exists? *repeats* nam)
       '()
       (let ([scmstr (struct->scm nam (read-fields nam flds))])
         (hash-table-set! *repeats* nam scmstr)
         scmstr))]
    [sexpr
      (unless (hash-table-exists? *repeats* sexpr)
        (hash-table-set! *repeats* sexpr #:ignored)
        (eprintf "; Ignoring definition ~s\n" sexpr))
      '()]))

(let loop ()
  (let ([r (read)])
    (unless (eq? r #!eof)
      (for-each pretty-print (typedef-struct->scm r))
      (loop))))

