
BEGIN {
    RS="extern DECLSPEC ";
    FS=";";
    ORS="\n";
}

{
    $0=$1;
    if (!/\<SDLCALL\>/) { next }
    # Not supporting callbacks
    if (/\<SDL_EventFilter\>/) { next }
    if (/\<SDL_HintCallback\>/) { next }
    if (/\<SDL_iPhoneSetAnimationCallback\>/) { next }
    if (/\<SDL_HitTest\>/) { next }
    if (/\<SDL_WindowsMessageHook\>/) { next }
    if (/\<SDL_LogOutputFunction\>/) { next }
    if (/\<SDL_TimerCallback\>/) { next }
    if (/\<SDL_SetMemoryFunctions\>/) { next }
    if (/\<SDL_GetMemoryFunctions\>/) { next }
    # Not public
    if (/\<SDL_ReportAssertion\>/) { next }
    # stdinc stuff
    if (/\<SDL_.*?Z_CAP\>/) { next }
    if (/\<SDL_iconv_t\>/) { next }
    if (/\<SDL_mem/) { next }
    if (/\<wchar_t\>/) { next }
    if (/\<SDL_qsort\>/) { next }
    # varargs
    if (/\<va_list\>/) { next }
    if (/\.\.\./) { next }
    # Ignore threading stuff
    if (/\<SDL_.*?[Tt]hread/) { next }
    if (/\<SDL_TLS/) { next }
    # Ignore assertion stuff?
    if (/\<SDL_.*?AssertionHandler\>/) { next }
    # Ignore atomic stuff
    if (/\<SDL_[Aa]tomic/) { next }
    # Ignore net stuff
    if (/\<SDLNet_/) { next }

    # FIXME Cannot return struct types from functions
    if (/\<SDL_GameControllerButtonBind\>/) { next }
    # FIXME Cannot pass struct types to functions
    if (/\<SDL_JoystickGUID\>/) { next }

    # Platform-specific
    if (/\<SDL_WinRT/) { next }
    if (/\<SDL_DX/) { next }
    if (/\<SDL_Android/) { next }
    if (/\<SDL_iPhone/) { next }
    # Windows
    if (/Direct3D/) { next }
    if (/\<SDL_RegisterApp\>/) { next }
    if (/\<SDL_UnregisterApp\>/) { next }

    # Fonts - rendering takes a SDL_Color
    # but chicken does not like that
    # TODO make a union of SDL_Color and int32
    if (/\<TTF_/) { next }

    # TODO SDL_mixer
    if (/\<Mix_/) { next }

    # TODO Vulkan
    if (/\<SDL_Vulkan/) { next }

    # Clean up multi-line declarations
    gsub(/\n/, " ");
    # Remove guff
    gsub(/\<SDLCALL\>/, "");
    # Normalise spacing around asterisks and parentheses
    gsub(/ *\* */, " * ");
    gsub(/ *\( */, " ( ");
    gsub(/ *\) */, " ) ");
    # Normalise spacing around commas, also they are not valid identifiers
    gsub(/ *, */, " \",\" ");
    # Special case: (void) arguments list should be ()
    gsub(/\( void \)/, "()");
    # Merge repeat spaces (superficial)
    gsub(/ +/, " ");
    # Multi-word type names
    gsub(/\<const \</, "const-");
    gsub(/\<unsigned \</, "unsigned-");
    # Turn "pointer * *" into "pointer**"
    gsub(/ \*/, "*");
    print "(" $0 ")";
}

