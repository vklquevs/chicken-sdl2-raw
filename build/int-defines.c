
#include <stdio.h>

#include <SDL.h>
#include <SDL_shape.h>
#include <SDL_syswm.h>

#ifdef SDL2_image
#include <SDL_image.h>
#endif
#ifdef SDL2_mixer
#include <SDL_mixer.h>
#endif
#ifdef SDL2_net
#include <SDL_net.h>
#endif
#ifdef SDL2_ttf
#include <SDL_ttf.h>
#endif

#define IS_INT(x) _Generic((x), \
        signed short: 1, \
        unsigned short: 1, \
        signed int: 1, \
        unsigned int: 1, \
        long int: 1, \
        unsigned long int: 1, \
        long long int: 1, \
        unsigned long long int: 1, \
        const signed short: 1, \
        const unsigned short: 1, \
        const signed int: 1, \
        const unsigned int: 1, \
        const long int: 1, \
        const unsigned long int: 1, \
        const long long int: 1, \
        const unsigned long long int: 1, \
        default: 0)

int main(int argc, char *argv[]) {
#include "int-defines.body.c"
    return 0;
}

