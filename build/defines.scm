
(let loop ()
  (let ([r (read)])
    (unless (eq? r #!eof)
      (pretty-print
        `(define ,r (foreign-value ,(symbol->string r) int)))
      (loop))))


