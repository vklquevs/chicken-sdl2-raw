
/^#define\>/ {
    name = $2;
    $1=$2="";
    gsub(/\s+/, "");
    val = $0;
    if (val == "") { next }
    if (name !~ /^SDL/) { next }
    if (name ~ /\(/) { next }
    if (/__attribute__/) { next }
    # Not right
    if (name ~ /\<SDL_INLINE\>/) { next }
    if (name ~ /\<SDL_assert_data\>/) { next }
    if (name ~ /\<SDL_assert_state\>/) { next }
    if (name ~ /\<SDL_Colour\>/) { next }
    if (name ~ /\<SDLTEST/) { next }
    if (name ~ /\<SDL_REVISION\>/) { next }
    if (name ~ /\<SDL_REVISION_NUMBER\>/) { next }
    print "if (IS_INT("name")) { printf(\""name"\\n\"); }";
}


