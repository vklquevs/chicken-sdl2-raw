
(define (split-list l at)
  (if (null? l) '()
    (let loop ([acc-all '()]
               [acc-one '()]
               [l l])
      (cond
        [(null? l)
         (reverse (cons (reverse acc-one) acc-all))]
        [(equal? (car l) at)
         (loop (cons (reverse acc-one) acc-all) '() (cdr l))]
        [#t
         (loop acc-all (cons (car l) acc-one) (cdr l))]))))

(define (decl-args->ffi args)
  (map car (split-list args ",")))

(define (decl->ffi decl)
  (let ([rty (car decl)]
        [name (cadr decl)]
        [args (decl-args->ffi (caddr decl))])
    `(define ,name
       (foreign-lambda ,rty ,(symbol->string name) ,@args))))

(let loop ()
  (let ([r (read)])
    (if (eq? r #!eof)
      #f
      (begin
        ; (display ";") (pretty-print r)
        (pretty-print (decl->ffi r))
        (loop)))))

