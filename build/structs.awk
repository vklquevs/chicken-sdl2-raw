
/^typedef (struct|union) SDL_/ && !/;$/ {
    if ($3 ~ /\<SDL_RWops\>/) { next }
    typedef = NR;
    gsub(/\<typedef\s+/, "typedef-");
    line = $0;
}

/^{/ && (typedef == NR - 1) {
    capture = 1;
    print "(" line;
}

!/^#/ && capture {

    # Remove type qualifiers
    gsub(/\<const\s+/, "const-");
    gsub(/\<unsigned\s+/, "unsigned-");

    # type *val -> type* val
    gsub(/\*/, " * ");
    gsub(/\s+/, " ");
    gsub(/ \*/, "*");

    # Multiple fields declared with a comma
    gsub(/\s*,\s*/, " \",\" ");

    gsub(/\s*;\s*/, " \";\" ");

    print;
}

/^}/ {
    if (capture) {
        print ")";
    }
    capture = 0;
}

