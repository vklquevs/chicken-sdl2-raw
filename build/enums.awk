
BEGIN {
    ORS="\n";
}

/^(typedef )?enum/ { capture = 1; }
/;/ { capture = 0; }

!/^#/ && capture {
    gsub(/[,=].*/, "");
    if (!/SDL/ && !/\<IMG/) { next; }
    if (/^{$/) { next }
    if (/^(typedef )?enum/) { next }
    if (NF < 1) { next }
    $0=$1;
    gsub(/^\s*/, "");
    if (!/^\w/) { next }
    gsub(/=.*/, "");
    # match(/\<(.*?)\>/, $1, arr);
    gsub(/\s+/, "", $1);
    print "(define "$1" (foreign-value \""$1"\" int))";

}

