
(module sdl2-raw *

(import
  foreign chicken scheme)

(foreign-declare "#include <SDL.h>")
(foreign-declare "#include <SDL_shape.h>")
(foreign-declare "#include <SDL_syswm.h>")

(cond-expand [SDL2_image (foreign-declare "#include <SDL_image.h>")] (else (begin)))
(cond-expand [SDL2_mixer (foreign-declare "#include <SDL_mixer.h>")] (else (begin)))
(cond-expand [SDL2_net (foreign-declare "#include <SDL_net.h>")] (else (begin)))
(cond-expand [SDL2_ttf (foreign-declare "#include <SDL_ttf.h>")] (else (begin)))

(include "lib/types.scm")
(include "lib/functions.scm")
(include "lib/enums.scm")
(include "lib/defines.scm")
(include "lib/syntax.scm")
(include "lib/structs.scm")
)

