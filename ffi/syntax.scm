
(define-syntax &c-field-dereferencer
  (er-macro-transformer
    (lambda (sexpr rename compare)
      (let* ([ty (cadr sexpr)]
             [func (symbol-append '|&c-field-ref:| ty)])
        ; (format (current-error-port) ";;; ~a => ~a\n" sexpr func)
        func))))

(define-syntax &c-field-setter
  (er-macro-transformer
    (lambda (sexpr rename compare)
      (let* ([ty (cadr sexpr)]
             [func (symbol-append '|&c-field-set:| ty)])
        ; (format (current-error-port) ";;; ~a => ~a\n" sexpr func)
        func))))

; (define-foreign-structure
;   C_struct_name ()
;   [field_one int #f]
;   [field_two C_another_struct #t]
;   ...)
; =>
; (define &sizeof:C_struct_name (foreign-type-size C_struct_name))
; (define-syntax
;   &field-type:C_struct_name
;   (syntax-rules
;     (field_one field_two)
;     [(_ field_one) 'int]
;     [(_ field_two) 'C_another_struct]
;     [(_ field_two inner ...) (&field-type:C_another_struct inner ...)]))
; (define &offsetof:C_struct_name.field_one
;   (foreign-value "offsetof(C_struct_name, field_one)" size_t))
; (define &offsetof:C_struct_name.field_two
;   (foreign-value "offsetof(C_struct_name, field_two)" size_t))
; (define-syntax
;   &offsetof:C_struct_name
;   (syntax-rules
;     (field_one field_two)
;     ([_ field_one] &offsetof:C_struct_name.field_one)
;     ([_ field_two] &offsetof:C_struct_name.field_two)
;     ([_ field_two inner ...]
;      (+ &offsetof:C_struct_name.field_two
;         (&offsetof:C_another_struct inner ...)))))
(define-syntax define-foreign-structure
  (er-macro-transformer
    (lambda (sexpr rename compare)
      (let*
        ([struct-name (cadr sexpr)]
         [options (caddr sexpr)]
         [struct-fields (cdddr sexpr)]
         [struct-field-name car]
         [struct-field-type cadr]
         [struct-field-inner? caddr]
         [def-sizeof
           `(define
              ,(symbol-append '|&sizeof:| struct-name)
              (foreign-type-size ,struct-name))]
         [def-field-type-body
           (map
             (lambda (sf)
               `(((_ ,(struct-field-name sf))
                  ',(struct-field-type sf))
                 ,@(if (struct-field-inner? sf)
                     `(((_ ,(struct-field-name sf) inner ...)
                        (,(symbol-append '|&field-type:| (struct-field-type sf))
                          inner ...)))
                     '())))
             struct-fields)]
         [def-field-type
           `(define-syntax
              ,(symbol-append '|&field-type:| struct-name)
              (syntax-rules
                ,(map struct-field-name struct-fields)
                ,@(apply append def-field-type-body)))]
         [def-each-offsetof
           (map
             (lambda (sf)
               `(define
                  ,(symbol-append '|&offsetof:| struct-name
                                  '|.| (struct-field-name sf))
                  (foreign-value
                    ,(string-append "offsetof("
                                    (symbol->string struct-name)
                                    ", "
                                    (symbol->string (struct-field-name sf))
                                    ")")
                    size_t)))
             struct-fields)]
         [def-offsetof-body
           (map
             (lambda (sf)
               `(((_ ,(struct-field-name sf))
                  ,(symbol-append '|&offsetof:| struct-name
                                  '|.| (struct-field-name sf)))
                 ,@(if (struct-field-inner? sf)
                     `(((_ ,(struct-field-name sf) inner ...)
                        (+ ,(symbol-append '|&offsetof:| struct-name
                                           '|.| (struct-field-name sf))
                           (,(symbol-append '|&offsetof:| (struct-field-type sf))
                             inner ...))))
                     '())))
             struct-fields)]
         [def-offsetof
           `(define-syntax
              ,(symbol-append '|&offsetof:| struct-name)
              (syntax-rules
                ,(map struct-field-name struct-fields)
                ,@(apply append def-offsetof-body)))]
         [all-defs
           `(,def-sizeof
              ,def-field-type
              ,@def-each-offsetof
              ,def-offsetof)])
        `(begin ,@all-defs)))))

; Public syntax

; Usage: (c-field-set! SDL_Event ev (mouse button) 6)
(define-syntax c-field-set!
  (er-macro-transformer
    (lambda (sexpr rename compare)
      ; (format (current-error-port) ";;; ~a\n" sexpr)
      (let* ([struct-type (cadr sexpr)]
             [val (caddr sexpr)]
             [fields (cadddr sexpr)]
             [newval (car (cddddr sexpr))]
             [offs
               (let ([get (symbol-append '|&offsetof:| struct-type)])
                 (cons get fields))]
             [rtype
               (let ([get (symbol-append '|&field-type:| struct-type)])
                 (cons get fields))]
             [setter `(&c-field-setter ,(eval rtype))]
             [stx
               `(,setter (pointer+ ,val ,(expand offs)) ,newval)])
        ; (format (current-error-port) ";;; => ~a\n" stx)
        stx))))

; Usage: (c-field-ref SDL_Event ev mouse button)
(define-syntax c-field-ref
  (er-macro-transformer
    (lambda (sexpr rename compare)
      ; (format (current-error-port) ";;; ~a\n" sexpr)
      (let* ([struct-type (cadr sexpr)]
             [val (caddr sexpr)]
             [fields (cdddr sexpr)]
             [offs
               (let ([get (symbol-append '|&offsetof:| struct-type)])
                 (cons get fields))]
             [rtype
               (let ([get (symbol-append '|&field-type:| struct-type)])
                 (cons get fields))]
             [dereference `(&c-field-dereferencer ,(eval rtype))]
             [stx
               `(,dereference (pointer+ ,val ,(expand offs)))])
        ; (format (current-error-port) ";;; => ~a\n" stx)
        stx))))

; Usage: (define btn (&c-field SDL_Event (mouse button)))
; (btn ev)
; (set! (btn ev) val)
(define-syntax c-field
  (syntax-rules
    ()
    [(_ ty fields ...)
     (getter-with-setter
       (lambda (x)
         (c-field-ref ty x fields ...))
       (lambda (x v)
         (c-field-set! ty x (fields ...) v)))]))

(define-syntax foreign-sizeof
  (er-macro-transformer
    (lambda (sexpr rename compare)
      (string->symbol
        (string-append "&sizeof:"
                       (symbol->string (cadr sexpr)))))))

(define-syntax let-foreign+
  (syntax-rules
    ()
    [(_ ([cvar ctype count] ...) body ...)
     (let ([cvar (allocate (* (foreign-sizeof ctype) count))] ...)
       (begin
         (set-finalizer! cvar free) ...)
       body ...)]))

(define-syntax let-foreign
  (syntax-rules
    ()
    [(_ ([cvar ctype] ...) body ...)
     (let-foreign+ ([cvar ctype 1] ...) body ...)]))

