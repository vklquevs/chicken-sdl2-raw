let
    pkgs = import <nixpkgs> {};
    matchable = pkgs.callPackage ./nix/matchable.nix { inherit pkgs; };
    srfi-37 = pkgs.callPackage ./nix/srfi-37.nix { inherit pkgs; };
in
with pkgs;
stdenv.mkDerivation {
    name = "chicken-sdl2-raw";
    buildInputs = [
        pkgconfig
        chicken
        matchable
        srfi-37
        SDL2
        SDL2_image
    ];
}

