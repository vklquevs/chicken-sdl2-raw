
TESTS := $(patsubst %.scm, %, $($(wildcard test/*.scm)))

LIBS := lib/sdl2-raw.o

PKG_MODULES := sdl2 \
	$(shell pkg-config --exists SDL2_image && echo SDL2_image) \
	$(shell pkg-config --exists SDL2_gfx && echo SDL2_gfx) \
	$(shell pkg-config --exists SDL2_net && echo SDL2_net) \
	$(shell pkg-config --exists SDL2_ttf && echo SDL2_ttf) \
	$(shell pkg-config --exists SDL2_mixer && echo SDL2_mixer)

CSI_FEATURES := $(addprefix -D, ${PKG_MODULES})

INC_FLAGS := $(shell pkg-config --cflags-only-I ${PKG_MODULES})
INC_PATH := $(INC_FLAGS:-I%=%)
INCLUDES := $(shell find $(INC_PATH) -name 'SDL*.h')

CFLAGS := $(shell pkg-config --cflags ${PKG_MODULES})
LDFLAGS := $(shell pkg-config --libs ${PKG_MODULES})

lib/:
	@mkdir -p $@

lib/functions.scm: lib/ $(INCLUDES) build/functions.awk build/functions.scm
	cat $(INCLUDES) | \
		awk -f build/functions.awk | \
		csi -qb build/functions.scm > $@

lib/enums.scm: lib/ $(INCLUDES) build/enums.awk
	(for i in $(INCLUDES); do \
		cpp $(CFLAGS) $$i | \
		awk -f build/enums.awk; \
	done) | sort | uniq > $@

lib/int-defines.h: lib/
	gcc -E -dM $(INCLUDES) | sort | uniq > $@

lib/int-defines.body.c: lib/ build/int-defines.awk lib/int-defines.h
	cat lib/int-defines.h | \
		awk -f build/int-defines.awk > $@

lib/int-defines: build/int-defines.c lib/ lib/int-defines.body.c
	gcc $(CFLAGS) $(LDFLAGS) $(CSI_FEATURES) -I lib/ $< -o $@

# Some #defines are actually aliases to functions.
lib/defines.scm: lib/ lib/int-defines lib/functions.scm
	lib/int-defines | \
		csi -qb build/defines.scm > $@

lib/structs.scm: lib/ $(INCLUDES) build/structs.awk build/structs.scm
	(for i in $(INCLUDES); do \
		cpp $(CFLAGS) $$i | \
		awk -f build/structs.awk; \
	done) | \
		csi -qb build/structs.scm > $@

lib/sdl2-raw.scm: lib/ ffi/sdl2-raw.scm ffi/types.scm ffi/syntax.scm lib/functions.scm lib/enums.scm lib/defines.scm lib/structs.scm
	cp ffi/sdl2-raw.scm lib/
	cp ffi/types.scm lib/
	cp ffi/syntax.scm lib/

%.so: %.scm
	csc $(CSI_FEATURES) -C "$(CFLAGS)" -L "$(LDFLAGS)" -J -s $< -o $@

%.import.scm: %.so
	if test -e $(notdir $@); then mv $(notdir $@) $(dir $@); fi

%.import.so: %.import.scm
	csc -s $< -o $@

%.a: %.scm
	csc $(CSI_FEATURES) -C "$(CFLAGS)" -L "$(LDFLAGS)" -J -c $< -o $(basename $@).o
	ar rc $@ $(basename $@).o

.PHONY: ffi
ffi: lib/sdl2-raw.so lib/sdl2-raw.import.so lib/sdl2-raw.a

.PHONY: clean
clean:
	rm -r lib

all: ffi
	cp lib/sdl2-raw.so lib/sdl2-raw.import.so lib/sdl2-raw.a .

.PHONY: install
install: all
	chicken-install

