{ pkgs }:
let
  name = "chicken-sdl2-raw-${version}";
  version = "0.1.0";
in
with pkgs;
eggDerivation {
  inherit name;

  src = fetchgit {
    url = "https://gitlab.com/vklquevs/chicken-sdl2-raw";
    rev = "REV";
    sha256 = "SHA256";
  };

  buildPhase = ''
    make ffi
  '';

  installPhase = ''
    mkdir -p $out/lib
    cp lib/*.so $out/lib
  '';

  buildInputs = [
    chicken
    SDL2
    SDL2_image
  ];
}

