{ pkgs }:
let
  eggname = "srfi-37";
  version = "1.3.1";
  name = "${eggname}-${version}";
in
with pkgs;
eggDerivation {
  inherit name;

  src = fetchegg {
    inherit version;
    name = eggname;
    sha256 = "1a2zdkdzrv15fw9dfdy8067fsgh4kr8ppffm8mc3cmlczrrd58cb";
  };

  buildInputs = [ ];
}






