{ pkgs }:
let
  eggname = "matchable";
  version = "3.7";
  name = "${eggname}-${version}";
in
with pkgs;
eggDerivation {
  inherit name;

  src = fetchegg {
    inherit version;
    name = eggname;
    sha256 = "1vc9rpb44fhn0n91hzglin986dw9zj87fikvfrd7j308z22a41yh";
  };

  buildInputs = [ ];
}





